A small Go library for parsing GPRMC strings from a GPS device.

A sample application:

    package main
    
    import "fmt"
    import gps "gitlab.com/madebyjeffrey/gps"
    
    func main() {
      str := "$GPRMC,025527.000,A,4218.5216,N,08303.8395,W,0.50,88.65,040315,,,A*4E"

      location, err := gps.Parse(str)
    
      if err != nil {
        fmt.Println(err)
        return
      }
    
      fmt.Printf("%.5f\n", location.Latitude)
      fmt.Printf("%.5f\n", location.Longitude)
      fmt.Println(location.DateTime)
    }


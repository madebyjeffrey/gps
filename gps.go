package gps

import "strings"
import "time"
import "math"
import "strconv"
import "errors"

// Stores a GPS location and time stamp.
type Location struct {
  Latitude  float64
  Longitude float64
  DateTime  time.Time
}

// Converts N,E,S,W directions into a multiplier -1 or 1.
func Direction(str string) (float64, error) {
  switch (str) {
  case "N", "E": return 1.0, nil
  case "S", "W": return -1.0, nil
  default: return math.NaN(), errors.New("Invalid direction")
  }
}

// Converts a GPS latitude string into a number.
func Latitude(lat string) (float64, error) {
  if (len(lat) != 9) {
    return math.NaN(), errors.New("Latitude strings are 9 characters long")
  }

  str_deg := lat[0:2]
  str_min := lat[2:]

  degrees, err := strconv.ParseFloat(str_deg, 64)

  if (err != nil) {
    return math.NaN(), errors.New("Unable to convert floating point numbers")
  }

  minutes, err := strconv.ParseFloat(str_min, 64)

  if (err != nil) {
    return math.NaN(), errors.New("Unable to convert floating point numbers")
  }

  return degrees + minutes/60, nil
}

// Converts a GPS longitude string into a number.
func Longitude(long string) (float64, error) {
  if (len(long) != 10) {
    return math.NaN(), errors.New("Longitude strings are 10 characters long")
  }

  str_deg := long[0:3]
  str_min := long[3:]

  degrees, err := strconv.ParseFloat(str_deg, 64)

  if (err != nil) {
    return math.NaN(), errors.New("Unable to convert floating point numbers")
  }

  minutes, err := strconv.ParseFloat(str_min, 64)

  if (err != nil) {
    return math.NaN(), errors.New("Unable to convert floating point numbers")
  }

  return degrees + minutes/60, nil
}

// Converts a GPS time and date string pair into a Time.
func Time(time_ string, date_ string) (*time.Time, error) {
  datetime, err := time.Parse("020106 150405", date_ + " " + time_)

  if err != nil {
    return nil, errors.New("Cannot parse time")
  }

  return &datetime, nil
}

// Parses a GPRMC string.
func Parse(rmc string) (*Location, error)   {
  values := strings.Split(rmc, ",")
  lat_dir := values[4]
  lat := values[3]
  long_dir := values[6]
  long := values[5]

  var date_ string = values[9]
  var time_ string = strings.TrimSuffix(values[1], ".000")

  var location Location

  datetime, err := Time(time_, date_)

  if err != nil {
    return nil, err
  }

  location.DateTime = *datetime

  latitude_dir, err := Direction(lat_dir)

  if err != nil {
    return nil, err
  }


  location.Latitude, err = Latitude(lat)

  if err != nil {
    return nil, err
  }

  location.Latitude *= latitude_dir

  longitude_dir, err := Direction(long_dir)
  location.Longitude, err = Longitude(long)

  if err != nil {
    return nil, err
  }

  location.Longitude *= longitude_dir

  return &location, nil
}

